package im.forsyth.lazync;

/**
 * Leave this realm, mortal. Turn back.
 */
public abstract class Hellfire<W> {

    public <R> W/*<R>*/ destruction(final Class<?> c, final String m,
                             final Class<?>[] params, final Object[] args) {
        throw new UnsupportedOperationException("Not implemented");
    }

    public <R> W/*<R>*/ destruction(final Object o, final String m,
                            final Class<?>[] params, final Object[] args) {
        throw new UnsupportedOperationException("Not implemented");
    }

}
