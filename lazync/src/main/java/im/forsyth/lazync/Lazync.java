package im.forsyth.lazync;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.HashSet;
import java.util.Set;

@Retention(RetentionPolicy.SOURCE)
@Target(ElementType.METHOD)
public @interface Lazync {

    class Wrapper {

        public static final String FUTURE_TASK = "java.util.concurrent.FutureTask";
        public static final String RX_SINGLE = "io.reactivex.Single";
        public static final String GUAVA_LISTENABLE_FUTURE_TASK
                = "com.google.common.util.concurrent.ListenableFutureTask";
        public static final String ANDROID_ASYNC_TASK = "android.os.AsyncTask";

        private static final Set<String> sSupportedSet = new HashSet<String>() {
            {
                add(FUTURE_TASK);
                add(RX_SINGLE);
                add(GUAVA_LISTENABLE_FUTURE_TASK);
                add(ANDROID_ASYNC_TASK);
            }
        };

        public static boolean isSupported(String fullyQualifiedClass) {
            if (fullyQualifiedClass == null) {
                throw new IllegalArgumentException("fullyQualifiedClass cannot be null");
            }
            return sSupportedSet.contains(fullyQualifiedClass);
        }
    }

    String wrapper() default Wrapper.FUTURE_TASK;
    String tag() default "Lazync";
    GeneratedClassType generatedClassType() default GeneratedClassType.STANDALONE_PREPEND_TAG;
    GenericParameterResolution genericParameterResolution()
            default GenericParameterResolution.SEARCH_SUPERCLASS;
}