package im.forsyth.lazync;

public enum GeneratedClassType {
    STANDALONE_PREPEND_TAG,
    STANDALONE_APPEND_TAG,
    STANDALONE_TAG_ONLY
    //TODO ANDROID_ASYNC_TASK_TAG_ONLY
}
