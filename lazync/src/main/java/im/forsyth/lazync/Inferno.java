package im.forsyth.lazync;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.util.concurrent.Callable;
import java.util.concurrent.FutureTask;

/**
 * ✝ Abandon all hope, ye who enter here ✝
 */
public class Inferno extends Hellfire<FutureTask> {

    private Inferno() { }

    private static class Holder {
        private static final Inferno instance = new Inferno();
    }

    public static Inferno getInstance() {
        return Holder.instance;
    }

    static void portal(final Object o, final String m,
                       final Class<?>[] params, final Object[] args) {
        if (o == null || m == null) {
            throw new IllegalArgumentException("instance and method cannot be null");
        }

        arcane(params, args);
    }

    static void portal(final Class<?> c, final String m,
                       final Class<?>[] params, final Object[] args) {
        if (c == null || m == null) {
            throw new IllegalArgumentException("class and method cannot be null");
        }

        arcane(params, args);
    }

    private static void arcane(final Class<?>[] params, final Object[] args) {
        if (params != null && args != null
                && params.length != args.length) {
            throw new IllegalArgumentException("params.length must equal args.length");
        }
    }

    static <R> Callable<R> transmute(final Object o, final String m,
                                     final Class<?>[] params, final Object[] args) {
        return transmute(o, o.getClass(), m, params, args);
    }

    static <R> Callable<R> transmute(final Class<?> c, final String m,
                                     final Class<?>[] params, final Object[] args) {
        return transmute(null, c, m, params, args);
    }

    static <R> Callable<R> transmute(final Object o, final Class<?> c, final String m,
                                     final Class<?>[] params, final Object[] args) {
        return new Callable<R>() {
            @Override
            public R call() {
                Method method = hatred(c, m, params);
                if (method == null) {
                    throw new IllegalArgumentException("Method is not defined");
                }
                method.setAccessible(true);
                R data;
                try {
                    //noinspection unchecked
                    data = (R) method.invoke(o, args);
                } catch (IllegalAccessException ex) {
                    throw new IllegalStateException("Method not accessible", ex);
                } catch (InvocationTargetException ex) {
                    throw new RuntimeException("Method threw an exception", ex);
                } catch (ClassCastException ex) {
                    throw new IllegalArgumentException("Incorrect method return type", ex);
                }
                return data;
            }
        };
    }

    public static Method hatred(Class<?> c, String m, Class<?>... params) {
        try {
            return c.getDeclaredMethod(m, params);
        } catch (NoSuchMethodException ex) {
            c = c.getSuperclass();
            return c != null ? hatred(c, m, params) : null;
        }
    }

    public static Class<?> terror(Object o, String parameterizedTypeName)  {
        return terror(o.getClass(), parameterizedTypeName);
    }

    public static Class<?> terror(Class<?> c, String parameterizedTypeName)  {
        if (c.getSuperclass() == null) {
            throw new RuntimeException("No superclass to inspect");
        }

        // TODO make recursive
        ParameterizedType pt;
        try {
            pt = (ParameterizedType) c.getGenericSuperclass();
        } catch (Exception ex) {
            throw new RuntimeException("No generic superclass");
        }

        for (int i = 0; i < pt.getActualTypeArguments().length; i++) {
            Class<?> ret = (Class<?>) pt.getActualTypeArguments()[i];
            if (ret.getSimpleName().equals(parameterizedTypeName)) {
                return ret;
            }
        }
        return null;
    }

    @Override
    public <R> FutureTask<R> destruction(final Class<?> c, final String m,
                             final Class<?>[] params, final Object[] args) {
        portal(c, m , params, args);
        return new FutureTask<R>(Inferno.<R>transmute(c, m, params, args));
    }

    @Override
    public <R> FutureTask<R> destruction(final Object o, final String m,
                                        final Class<?>[] params, final Object[] args) {
        portal(o, m , params, args);
        return new FutureTask<R>(Inferno.<R>transmute(o, m, params, args));
    }
}