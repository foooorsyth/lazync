package im.forsyth.lazync;

import io.reactivex.Single;

public class RxInferno extends Hellfire<Single> {

    private RxInferno() { }

    private static class Holder {
        private static final RxInferno instance = new RxInferno();
    }

    public static RxInferno getInstance() {
        return Holder.instance;
    }

    @Override
    public <R> Single<R> destruction(final Class<?> c, final String m,
                                    final Class<?>[] params, final Object[] args) {
        Inferno.portal(c, m, params, args);
        return Single.fromCallable(Inferno.<R>transmute(c, m, params, args));
    }

    @Override
    public <R> Single<R> destruction(final Object o, final String m,
                                        final Class<?>[] params, final Object[] args) {
        Inferno.portal(o, m, params, args);
        return Single.fromCallable(Inferno.<R>transmute(o, m, params, args));
    }
}
