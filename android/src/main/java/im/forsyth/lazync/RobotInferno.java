package im.forsyth.lazync;

import android.os.AsyncTask;

public class RobotInferno extends Hellfire<AsyncTask> {

    private RobotInferno() { }

    private static class Holder {
        private static final RobotInferno instance = new RobotInferno();
    }

    public static RobotInferno getInstance() {
        return Holder.instance;
    }

    @Override
    public <R>
    AsyncTask<LazyncTask.Callback<R>, Void, R> destruction(final Class<?> c, final String m,
                                                   final Class<?>[] params, final Object[] args) {
        Inferno.portal(c, m, params, args);
        return new LazyncTask<>(c, m, params, args);
    }

    @Override
    public <R>
    AsyncTask<LazyncTask.Callback<R>, Void, R> destruction(final Object o, final String m,
                                                   final Class<?>[] params, final Object[] args) {
        Inferno.portal(o, m, params, args);
        return new LazyncTask<>(o, m, params, args);
    }
}
