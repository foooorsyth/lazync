package im.forsyth.lazync;

import android.os.AsyncTask;

public class LazyncTask<R> extends AsyncTask<LazyncTask.Callback<R>, Void, R> {

    public interface Callback<R> {
        void done(R result);
        void error(Throwable thr);
        void cancelled(R result);
    }

    private Object mO;
    private Class<?> mC;
    private String mM;
    private Class<?>[] mParams;
    private Object[] mArgs;
    private LazyncTask.Callback<R> mCb = null;
    private boolean mError;
    private Throwable mThr;

    LazyncTask(final Object o, final String m,
                      final Class<?>[] params, final Object[] args) {
        mO = o;
        mC = mO.getClass();
        mM = m;
        mParams = params;
        mArgs = args;
    }

    LazyncTask(final Class<?> c, final String m,
               final Class<?>[] params, final Object[] args) {
        mO = null;
        mC = c;
        mM = m;
        mParams = params;
        mArgs = args;
    }

    @Override
    protected R doInBackground(LazyncTask.Callback<R>... cb) {
        if (cb != null && cb.length > 0 && cb[0] != null) {
            mCb = cb[0];
        }
        try {
            return Inferno.<R>transmute(mO, mC, mM, mParams, mArgs).call();
        } catch (Throwable thr) {
            mError = true;
            mThr = thr;
            return null;
        }
    }

    @Override
    protected void onPostExecute(R r) {
        if (mCb != null) {
            if (!mError) {
                mCb.done(r);
            } else {
                mCb.error(mThr);
            }
        }
    }

    @Override
    protected void onCancelled(R r) {
        super.onCancelled(r);
        if (mCb != null) {
            mCb.cancelled(r);
        }
    }

}
