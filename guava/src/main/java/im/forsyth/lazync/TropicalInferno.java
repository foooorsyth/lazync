package im.forsyth.lazync;

import com.google.common.util.concurrent.ListenableFutureTask;

public class TropicalInferno extends Hellfire<ListenableFutureTask> {

    private TropicalInferno() { }

    private static class Holder {
        private static final TropicalInferno instance = new TropicalInferno();
    }

    public static TropicalInferno getInstance() {
        return Holder.instance;
    }


    @Override
    public <R> ListenableFutureTask<R> destruction(final Class<?> c, final String m,
                                         final Class<?>[] params, final Object[] args) {
        Inferno.portal(c, m , params, args);
        return ListenableFutureTask.create(Inferno.<R>transmute(c, m, params, args));
    }

    @Override
    public <R> ListenableFutureTask<R> destruction(final Object o, final String m,
                                                   final Class<?>[] params, final Object[] args) {
        Inferno.portal(o, m, params, args);
        return ListenableFutureTask.create(Inferno.<R>transmute(o, m, params, args));
    }

}
