package im.forsyth.lazync.test;

import android.util.Log;

import im.forsyth.lazync.Lazync;

public class SampleClass {

    private static final String TAG = "SampleClass";

    @Lazync/*(wrapper = Lazync.Wrapper.FUTURE_TASK) // this is the default wrapper*/
    public void futureTask() {
        Log.d(TAG, "FutureTask");
    }

    @Lazync(wrapper = Lazync.Wrapper.RX_SINGLE)
    public void rxSingle() {
        Log.d(TAG, "RxSingle");
    }

    @Lazync(wrapper = Lazync.Wrapper.GUAVA_LISTENABLE_FUTURE_TASK)
    public void listenableFutureTask() {
        Log.d(TAG, "ListenableFutureTask");
    }

    @Lazync(wrapper = Lazync.Wrapper.ANDROID_ASYNC_TASK)
    public void asyncTask() {
        Log.d(TAG, "asyncTask");
    }
}
